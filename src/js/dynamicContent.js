



$(document).ready(function () {
    $('#show-modal').on('show.bs.modal', function (e) {
        let button = $(e.relatedTarget); // Button that triggered the modal
        let titleModal = button.attr('data-title');
        let productCostNow = button.attr('data-product-cost');
        let imgProducts = button.attr('data-src');
        let productSale = button.attr('data-sale');// Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        let modalShow = $(this);
        modalShow.find('.old-price').text(productSale);
        modalShow.find('.modal-show__title').text(titleModal);
        modalShow.find('.product-cost-now').text(productCostNow);
        modalShow.find('.modal-img').attr('src',imgProducts)
    });
});