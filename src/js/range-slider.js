function rangeSlider() {
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 0, 300 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
        " - $" + $( "#slider-range" ).slider( "values", 1 ) );
    $( "#modal-slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 0, 300 ],
        slide: function( event, ui ) {
            $( "#modal-amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( "#modal-amount" ).val( "$" + $( "#modal-slider-range" ).slider( "values", 0 ) +
        " - $" + $( "#modal-slider-range" ).slider( "values", 1 ) );
} ;

rangeSlider();