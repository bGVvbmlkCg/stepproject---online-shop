let basket = {
    basketIcon: '.header__basket',
    storageName: "basket",
    emptyBanner: "#cardIsEmpty",
    itemCount: '.basket-itemCount',
    basketFooter: "#modalFooter",
    itemsContainer: "#cardItems",
    totalSum: '.totalPrice__value',
    items: [],
    itemHTML: '<div class="row border cardItems__item mb-4 rounded"><div class="col-lg-3 cardItems__item__image col-6 col-md-4"><img alt="Item 1"class="basket-itemImage img-fluid"src=./img/furniture_gallery_items/14.png></div><div class="cardItems__item__body col"><p class="m-0 basket-itemTitle cardItems__item__title py-2"><div class=cardItems__details><div class="row justify-content-between"><div class="col-12 col-lg-4"><span class=cardItems__item__price><del class="mr-1 basket-itemLastPrice lastPrice"></del> <span class="p-1 basket-itemPrice currentPrice"></span></span></div><div class="col-8 col-lg-5 d-md-block d-none m-lg-0 mt-md-3"><div class="align-items-end cardItems__item__quantity d-flex quantity"><label class="m-0 quantity__name"><span class="m-0 mr-lg-3">Quantity:</span> <input class="mr-1 basket-itemCount mr-lg-2 quantity__itemCount"disabled></label> <button class="fas fa-minus mr-1 quantity__minus"onclick=\'basket.onClickCount(this,"-")\'type=button></button> <button class="fas fa-plus quantity__plus"onclick=\'basket.onClickCount(this,"+")\'type=button></button></div></div><div class="col-4 col-lg-3 d-md-block d-none mt-lg-0 mt-md-3"><div class="cardItems__item__sum sum text-center"><p class="mb-2 sum__title">Sum<p class="basket-itemSum sum__price"></div></div></div></div></div><div class="container m-md-0 mt-4"><div class="row d-md-none"><div class=col-8><div class="align-items-end cardItems__item__quantity d-flex quantity"><label class="m-0 quantity__name"><p class="mb-1 mr-3">Quantity:</p><input class="basket-itemCount quantity__itemCount mr-2 p-1"disabled></label> <button class="fas fa-minus mr-1 quantity__minus px-2"onclick=\'basket.onClickCount(this,"-")\'type=button></button> <button class="fas fa-plus quantity__plus px-2"onclick=\'basket.onClickCount(this,"+")\'type=button></button></div></div><div class=col-4><div class="cardItems__item__sum sum text-center"><p class="mb-2 sum__title">Sum<p class="basket-itemSum sum__price"></div></div></div></div></div>',
    countElements: function () {
        return this.items.length;
    },
    getItems: function () {
        let storageItems = localStorage.getItem(this.storageName);
        if (!storageItems) {
            storageItems = '[]'
        }
        this.items = JSON.parse(storageItems);
        return this.items;
    },
    addItem: function (item) {
        this.addToStorage(item.image, item.title, item.lastPrice, item.price, item.count, item.id);
        this.displayItem(item.image, item.title, item.lastPrice, item.price, item.count, item.id);
    },
    clear: function () {
        $(this.itemsContainer).find('.cardItems__item').remove();
        localStorage.removeItem(this.storageName);
        this.items = [];
        this.displayBasket();
    },
    displayItem: function(image, title, lastPrice, price, count, id) {
        // Add to HTML
        let $HTML = $(this.itemHTML);
        $HTML.attr('data-id', id);
        $HTML.find('.basket-itemImage').attr('src', image);
        $HTML.find('.basket-itemTitle').text(title);
        if (lastPrice) {
            $HTML.find('.basket-itemLastPrice').text(lastPrice);
        } else {
            $HTML.find('.basket-itemLastPrice').remove();
        }
        $HTML.find('.basket-itemPrice').text(price);
        $HTML.find('.basket-itemCount').val(count);
        $HTML.find('.basket-itemSum').text(count * price);
        $(this.itemsContainer).prepend($HTML);
    },
    addToStorage: function (image, title, lastPrice,price, count, id) {
        this.items.push({image, title, lastPrice, price, count, id});
        localStorage.setItem(this.storageName, JSON.stringify(this.items));
    },
    updateDisplayMode: function () {
        if (this.items.length) {
            $('#cardIsEmpty').removeClass('d-block').addClass('d-none');
            $('#modalFooter').removeClass('d-none').addClass('d-block');
            $('#cardItems').removeClass('d-none').addClass('d-block');
        } else {
            $('#cardIsEmpty').removeClass('d-none').addClass('d-block');
            $('#modalFooter').removeClass('d-block').addClass('d-none');
            $('#cardItems').removeClass('d-block').addClass('d-none');
        }
    },
    updateBasketCount: function () {
        $(this.basketIcon).attr('data-count-items', this.countElements())
    },
    updateStorage: function() {
        localStorage.setItem(this.storageName, JSON.stringify(this.items));
    },
    updateSum: function ($itemContainer, count) {
        let $sum = $itemContainer.find('.basket-itemSum');
        let $price = $itemContainer.find('.basket-itemPrice');
        $sum.text($price.text() * count);
    },
    updateTotalSum: function () {
        let totalSum = $(this.itemsContainer).find('.basket-itemSum').toArray().reduce( (lastValue, value) => +value.innerText + lastValue, 0) / 2;
        $(this.totalSum).text(totalSum);
    },
    displayBasket: function () {
        let itemsToDisplay = this.getItems();
        if (itemsToDisplay.length) {
            itemsToDisplay.forEach( item => this.displayItem(item.image, item.title, item.lastPrice, item.price, item.count, item.id));
        }
        this.updateDisplayMode();
        this.updateBasketCount();
        this.updateTotalSum();
    },
    isNotItemInBasket: function (ID) {
        return !this.items.some( item => item.id === ID);
    },
    onClickToAdd: function (button, container, wrapperFunction) {
        let item = wrapperFunction(button, container);
        if (item && this.isNotItemInBasket(item.id)) {
            this.addItem(item);
            this.updateBasketCount();
            this.updateDisplayMode();
            this.updateTotalSum();
        }
    },
    onClickCount: function (button, operation) {
        let $itemContainer = $(button).closest('.cardItems__item');
        let $itemCount = $itemContainer.find(this.itemCount);
        let value = $itemCount.val();
        if (operation === '+') {
            ++value;
        } else if (operation === '-') {
            --value;
        } else {
            console.log('INVALID OPERATION NAME!')
        }
        if (value >= 0) {
            $itemCount.val(value);
            let ID = $itemContainer.attr('data-id');
            this.items.forEach( item => {
                if (item.id === ID) {
                    item.count = value;
                }
            });
            this.updateStorage();
            this.updateSum($itemContainer, value);
            this.updateTotalSum();
        }
    }
};
basket.displayBasket();

function addToBasketFromGallery(button, container) {
    let $itemContainer = $(button).closest(container);
    return {
        id: $itemContainer.find('.b-title').text() + $itemContainer.find('.b-image').attr('src'),
        image: $itemContainer.find('.b-image').attr('src'),
        title: $itemContainer.find('.b-title').text(),
        lastPrice: $itemContainer.find('.b-lastPrice').text(),
        price: $itemContainer.find('.b-price').text(),
        count: 1,
    };
}

function addToBasketFromProducts(button, container) {
    let $itemContainer = $(button).closest(container);
    return {
        id: $itemContainer.find('.button-view').attr('data-title') + $itemContainer.find('.button-view').attr('data-src'),
        image: $itemContainer.find('.button-view').attr('data-src'),
        title: $itemContainer.find('.button-view').attr('data-title'),
        lastPrice: $itemContainer.find('.button-view').attr('data-sale').replace('$', ''),
        price: $itemContainer.find('.button-view').attr('data-product-cost').replace('$', ''),
        count: 1,
    }
}

function addToBasketProductModal(button, container) {
    let $itemContainer = $(button).closest(container);
    return {
        id: $itemContainer.find('.b-title').text() + $itemContainer.find('.b-image').attr('src'),
        image: $itemContainer.find('.b-image').attr('src'),
        title: $itemContainer.find('.b-title').text(),
        lastPrice: $itemContainer.find('.b-lastPrice').text().replace('$', ''),
        price: $itemContainer.find('.b-price').text().replace('$', ''),
        count: 1,
    };
}